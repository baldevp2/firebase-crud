<?php

namespace App\Http\Controllers;

use Image;
use Illuminate\Http\Request;
use Auth;
use Storage;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('realtimecrud');
    }

    public function uploadFile(Request $request)
    {
        // dd(Auth::user());
        $data['body'] = $request->body;
        $data['title'] = $request->title;
        if ($request->hasFile('file')) {
            $avatar = $request->file('file');
            $extension = $request->file('file')->getClientOriginalExtension();
            $filename = md5(time()).'_'.$avatar->getClientOriginalName();
            $normal = Image::make($avatar)->resize(540, 540)->encode($extension);
            $medium = Image::make($avatar)->resize(200, 200)->encode($extension);
            $small = Image::make($avatar)->resize(40, 40)->encode($extension);
            Storage::disk('local')->put('/users/avatar/normal/'.Auth::user()->uuid.'_'.$filename, (string) $normal, 'public');
            // Storage::disk('local')->put('/users/avatar/medium/'.Auth::user()->uuid.'-'.$filename, (string) $medium, 'public');
            Storage::disk('local')->put('/users/avatar/small/'.Auth::user()->uuid.'_'.$filename, (string) $small, 'public');

            $data['normal_url'] = Storage::url('/users/avatar/normal/'.Auth::user()->uuid.'_'.$filename);
            // $data['mediumUrl'] = Storage::url('users/avatar/medium/'.$filename);
            $data['small_url'] = Storage::url('/users/avatar/small/'.Auth::user()->uuid.'_'.$filename);
        }

        return $data;
    }
}
