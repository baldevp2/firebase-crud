@extends('layouts.app')

@section('style')
<style type="text/css">
    .desabled {
        pointer-events: none;
    }
</style>
@endsection

@section('content')
<div class="container">
    <div class="row">
    	<div class="col-md-4">
    		<div class="card card-default">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            <strong>Add New Blog</strong>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <form id="addUser" class="" method="POST"  enctype="multipart/form-data">
                        {{csrf_field()}}
                    	<div class="form-group">
                            <label for="body" class="col-md-12 col-form-label">Blog Body</label>

                            <div class="col-md-12">
                                <input id="body" type="text" class="form-control" name="body" value="" required autofocus>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="title" class="col-md-12 col-form-label">title</label>

                            <div class="col-md-12">
                                <input id="title" type="text" class="form-control" name="title" value="" required autofocus>
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="file"></label>
                            <div class="col-md-12">
                                <input type="file" name="file">
                            </div>
                        </div>
                        <br>
                        <div class="form-group ">
                            <div class="col-md-12 ">
                                <button type="submit" class="btn btn-primary btn-block desabled" id="submitUser">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
    	</div>
        <div class="col-md-8">
            <div class="card card-default">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            <strong>All Blog Listing</strong>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>Image</th>
                            <th>Body</th>
                            <th>Title</th>
                            <th width="180" class="text-center">Action</th>
                        </tr>
                        <tbody id="tbody">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Delete Model -->
<form action="" method="POST" class="users-remove-record-model">
    <div id="remove-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" style="width:55%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="custom-width-modalLabel">Delete Record</h4>
                    <button type="button" class="close remove-data-from-delete-form" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <h4>You Want You Sure Delete This Record?</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect remove-data-from-delete-form" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger waves-effect waves-light deleteMatchRecord">Delete</button>
                </div>
            </div>
        </div>
    </div>
</form>

<!-- Update Model -->
<form action="" method="POST" class="users-update-record-model form-horizontal">
    <div id="update-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" style="width:55%;">
            <div class="modal-content" style="overflow: hidden;">
                <div class="modal-header">
                    <h4 class="modal-title" id="custom-width-modalLabel">Update Record</h4>
                    <button type="button" class="close update-data-from-delete-form" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" id="updateBody">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect update-data-from-delete-form" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success waves-effect waves-light updateUserRecord">Update</button>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection

@section('scripts')
<script src="{{asset('js/firebase.js')}}"></script>
<script>
// Initialize Firebase
var config = {
    apiKey: "{{ config('services.firebase.api_key') }}",
    authDomain: "{{ config('services.firebase.auth_domain') }}",
    databaseURL: "{{ config('services.firebase.database_url') }}",
    {{--  storageBucket: "{{ config('services.firebase.storage_bucket') }}",  --}}
};
firebase.initializeApp(config);

var database = firebase.database();
var lastIndex = 0;

// Get Data
database.ref('blog/posts').on('value', function(snapshot) {
    var value = snapshot.val();
    console.log(value);
    var htmls = [];
    $.each(value, function(index, value){
    	if(value) {
    		htmls.push('<tr>\
                 <td><img src='+value.small_url+'></td>\
        		<td>'+ value.body +'</td>\
        		<td>'+ value.title +'</td>\
        		<td><a data-toggle="modal" data-target="#update-modal" class="btn btn-outline-success updateData" data-id="'+index+'">Update</a>\
        		<a data-toggle="modal" data-target="#remove-modal" class="btn btn-outline-danger removeData" data-id="'+index+'">Delete</a></td>\
        	</tr>');
    	}
    	lastIndex = index;
    });
    $('#tbody').html(htmls);
    $("#submitUser").removeClass('desabled');
});

//form Submit
$(document).on('submit', 'form#addUser',function(e){
    e.preventDefault(); // does not go through with the link.
    $.ajaxSetup({
        headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var userID = lastIndex+1;
    var formData = new FormData($(this)[0]);
        $.ajax({
        url: "{{route('fileupload')}}",
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        enctype: 'multipart/form-data',
        processData: false,
        success: function (response) {
            var normal_url = response.normal_url?response.normal_url:'';
            var small_url = response.small_url?response.small_url:'';
            database.ref('blog/posts/' + userID).set({
                body: response.body,
                title: response.title,
                normal_url:normal_url,
                small_url:small_url,
            });
            lastIndex = userID;
            $("#addUser input").val("");
        }
    });
    return false;
});



{{-- // Add Data
$('#submitUser').on('click', function(evt){
    evt.preventDefault();
    var values = $("#addUser").serializeArray();
    var body = $("#addUser #body").val();
	var title =  $("#addUser #title").val();
    var userID = lastIndex+1;
      database.ref('blog/posts/' + userID).set({
        body: body,
        title: title,
    });
    // Reassign lastID value
    lastIndex = userID;
    $("#addUser input").val("");
    return false;
}); --}}

// Update Data
var updateID = 0;
$('body').on('click', '.updateData', function() {
	updateID = $(this).attr('data-id');
	database.ref('blog/posts/' + updateID).on('value', function(snapshot) {
		var values = snapshot.val();
		var updateData = '<div class="form-group">\
		        <label for="body" class="col-md-12 col-form-label">Body</label>\
		        <div class="col-md-12">\
		            <input id="body" type="text" class="form-control" name="body" value="'+values.body+'" required autofocus>\
		        </div>\
		    </div>\
		    <div class="form-group">\
		        <label for="title" class="col-md-12 col-form-label">Title</label>\
		        <div class="col-md-12">\
                    <input id="title" type="text" class="form-control" name="title" value="'+values.title+'" required autofocus>\
                    <input id="normal_url" type="hidden" class="form-control" name="normal_url" value="'+values.normal_url+'" required autofocus>\
                <input id="small_url" type="hidden" class="form-control" name="small_url" value="'+values.small_url+'" required autofocus>\
		        </div>\
            </div>';
		    $('#updateBody').html(updateData);
	});
});

$('.updateUserRecord').on('click', function() {
	var values = $(".users-update-record-model").serializeArray();
	var postData = {
	    body : $('.users-update-record-model #body').val(),
        title : $('.users-update-record-model #title').val(),
        normal_url:$('.users-update-record-model #normal_url').val(),
        small_url:$('.users-update-record-model #small_url').val(),
	};
	var updates = {};
	updates['/blog/posts/' + updateID] = postData;
	firebase.database().ref().update(updates);
	$("#update-modal").modal('hide');
});


// Remove Data
$("body").on('click', '.removeData', function() {
	var id = $(this).attr('data-id');
	$('body').find('.users-remove-record-model').append('<input name="id" type="hidden" value="'+ id +'">');
});

$('.deleteMatchRecord').on('click', function(){
	var values = $(".users-remove-record-model").serializeArray();
	var id = values[0].value;
	firebase.database().ref('blog/posts/' + id).remove();
    $('body').find('.users-remove-record-model').find( "input" ).remove();
	$("#remove-modal").modal('hide');
});
$('.remove-data-from-delete-form').click(function() {
	$('body').find('.users-remove-record-model').find( "input" ).remove();
});
</script>
@endsection