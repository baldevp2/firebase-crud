<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'firebase' => [
        'api_key' => 'AIzaSyAPKBN3qQm7lbOFM_2d0lDDAUm6zbjm5sQ', // Only used for JS integration
        'auth_domain' => 'laravelfirebase-4d46a.firebaseapp.com', // Only used for JS integration
        'database_url' => 'https://laravelfirebase-4d46a.firebaseio.com',
        'secret' => 'pZ9S3thQzdWXjTtVzge1ALM1HghAl9mtmScZuf0g',
        'projectId'=>'laravelfirebase-4d46a',
        'storage_bucket' => 'laravelfirebase-4d46a.appspot.com', // Only used for JS integration
        'messagingSenderId'=>'198851465836',
    ],

];
